#!/usr/bin/env python3

import datetime
import requests
import yaml
import os
import sys
import logging

import l1
import l1.calendar

LOGGER = logging.getLogger("l1")
LOGGER.setLevel(logging.INFO)
LOGGER.addHandler(logging.StreamHandler())

session = requests.Session()

CONFIG = os.environ.get("L1_CONFIG", "l1_classes.yml")
USER = os.environ.get("L1_USER", None)
PASS = os.environ.get("L1_PASS", None)

if not (USER and PASS):
    print(
        "Error: Missing Auth details. Please Set both L1_USER and L1_PASS",
        file=sys.stderr,
    )
    sys.exit(1)

with open(CONFIG, "r") as fd:
    config = yaml.load(fd)

login_params = {
    "log": USER,
    "pwd": PASS,
    "action": "login",
    "wp-submit": "Log In",
    "redirect_to": "https://www.l1performance.com/book-a-class",
}

login_url = "https://www.l1performance.com/login/"
r = session.post(login_url, data=login_params)
r.raise_for_status()

classes_to_book = []
classes = l1.ListClasses(session=session)

for cls in config["classes"]:
    classes_to_book.extend(
        classes.filter_classes(name=cls["name"], day=cls["day"], time=cls["time"])
    )

my_classes = l1.MyClasses(session=session)
now = datetime.datetime.now()

for cls in classes_to_book:
    if cls["start"] > now:
        success = my_classes.book_class(cls["url"])
        if success:
            cfg = config.get("calendar", {})
            for x in ["cal_name", "cal_url", "cal_pass", "cal_user"]:
                if x not in cfg.keys():
                    cfg[x] = os.environ.get(x.upper(), "None")

            vevent = l1.calendar.Vevent(
                org_name="L1 Performance Ltd",
                org_email="reception@l1performance.com",
                start=cls["start"],
                end=cls["end"],
                summary=cls["title"],
                location="L1 Performance, Unit 2, 6 Wellington Place, Leeds, LS1 4AP",
            )
            cal = l1.calendar.Calendar(
                calendar=cfg["cal_name"],
                url=cfg["cal_url"],
                user=cfg["cal_user"],
                password=cfg["cal_pass"],
            )
            try:
                cal.add_event(str(vevent))
            except Exception as exp:
                LOGGER.error(
                    "Unable to add calendar event for %s at %s: %s",
                    cls["title"],
                    cls["start"],
                    exp,
                )
