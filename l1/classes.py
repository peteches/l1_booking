import sys
import time
import re
import urllib
import logging
import datetime
import requests
import bs4

LOGGER = logging.getLogger('l1')
L1_URL = 'https://www.l1performance.com'


class ListClasses:
    class_url = '{}/wp-admin/admin-ajax.php'.format(L1_URL)

    def __init__(self, session=None):
        self._classes = []
        self.now = datetime.datetime.now()
        self.in2weeks = datetime.timedelta(weeks=2) + self.now
        self.start = datetime.datetime.strftime(self.now, "%Y-%m-%d")
        self.end = datetime.datetime.strftime(self.in2weeks, "%Y-%m-%d")

        self.session = session or requests.Session()

        self.params = {
            'action': 'WP_FullCalendar',
            'type': 'event',
            'start': self.start,
            'end': self.end,
        }

    @property
    def known_classes(self):
        if self._classes:
            return self._classes

        resp = self.session.get(self.class_url, params=self.params)
        for cls in resp.json():
            LOGGER.debug("found class %s at %s", cls['title'], cls['start'])
            cls['start'] = datetime.datetime.strptime(cls['start'],
                                                      "%Y-%m-%dT%H:%M:%S")
            cls['end'] = datetime.datetime.strptime(cls['end'],
                                                    "%Y-%m-%dT%H:%M:%S")
            cls['url'] = urllib.parse.urlparse(cls['url'].split(':', 1)[-1], scheme='https')
            self._classes.append(cls)

        return self._classes

    def cls_on_day(self, cls, day):
        if day.lower()[0:3] == cls['start'].strftime('%a').lower()[0:3]:
            return True
        return False

    def cls_starts_at(self, cls, time):
        start_time = datetime.datetime.strptime(time,
                                                "%H:%M")
        if start_time.hour == cls['start'].hour \
                and start_time.minute == cls['start'].minute:
            return True

        return False

    def cls_is_called(self, cls, name):
        if name.lower() in cls['title'].lower():
            return True
        return False

    def filter_classes(self, name=None, day=None, time=None):

        if name is None \
                and day is None \
                and time is None:
            raise "No filters supplied"

        rv = []

        for cls in self.known_classes:
            if name is None \
                    or self.cls_is_called(cls, name):
                if day is None \
                        or self.cls_on_day(cls, day):
                    if time is None \
                           or self.cls_starts_at(cls, time):
                        rv.append(cls)

        return rv


class MyClasses:

    my_classes_url = '{}/my-account/my-classes'.format(L1_URL)

    def __init__(self, session=None):
        self.session = session or requests.Session()
        self._classes = []


    @property
    def all_my_classes(self):
        if self._classes:
            return self._classes
        resp = self.session.get(self.my_classes_url)
        html = resp.text
        soup = bs4.BeautifulSoup(html, 'html.parser')
        table = soup.find(id='dbem-bookings-table')
        booked_classes = table.findAll(lambda t: t.name == 'tr')
        for cls in booked_classes[1:]:
            cols = cls.findAll(lambda t: t.name == 'td')
            self._classes.append({
                'name': cols[0].text.strip(),
                'url': urllib.parse.urlparse(cols[0].find('a').get('href').split(':', 1)[-1], scheme='https'),
                'start_time': datetime.datetime.strptime(cols[1].text,
                                                         "%d/%m/%Y @ %H:%M:%S"),
                'status': cols[3].text.strip(),
                'action': cols[4].text.strip(),
            })

        return self._classes

    @property
    def upcoming_classes(self):

        now = datetime.datetime.now()

        return [cls for cls in self.all_my_classes
                if cls['start_time'] > now]


    @property
    def finished_classes(self):
        now = datetime.datetime.now()

        return [cls for cls in self.all_my_classes
                if cls['start_time'] < now]


    def book_class(self, class_url):

        booking_url = '{}/wp-admin/admin-ajax.php'.format(L1_URL)

        for cls in self.all_my_classes:
            if class_url.path in cls['url'].path:
                LOGGER.info('%-25s on %-20s has been booked and is %s', cls['name'], cls['start_time'].strftime('%a %d %b at %X'), cls['status'])
                return True

        resp = self.session.get(class_url.geturl())
        html = resp.text
        soup = bs4.BeautifulSoup(html, 'html.parser')

        form = soup.find('form')

        class_name = ""
        class_start = ""

        if form is None:
            if 'You are currently attending this class' in  soup.find(id='em-booking').text:
                LOGGER.info("You are already attending %s at %s", class_name, class_start)
                return True
            else:
                return False
        hidden_form_inputs = form.findAll(lambda t: t.name == 'input' and t.get('type') == 'hidden')

        params = {}

        for input in hidden_form_inputs:
            params[input.get('name')] = input.get('value')

        resp = self.session.post(booking_url,
                                data=params)

        try:
            resp.raise_for_status()
        except Exception as e:
            LOGGING.error("Failed to book class %s at %s: %s", class_name, class_start, e)
            return false
        else:
            LOGGING.info("Successfully booked class %s at %s.", class_name, class_start)
        finally:
            return resp.ok


class CreateClass:

    add_class_url = '{}/wp-admin/post-new.php?post_type=event'.format(L1_URL)
    submit_class_form_url = '{}/wp-admin/post.php'.format(L1_URL)
    ajax_url = '{}/wp-admin/admin-ajax.php'.format(L1_URL)

    def __init__(self,
                 session=None,
                 name=None,
                 starttime=None,
                 endtime=None,
                 description=None,
                 instructor=None,
                 category=None,
                 spaces=None,
                 ):

        if not name:
            raise ArgumentError("Expected name to be truthy, got: " +  name)
        if not starttime:
            raise ArgumentError("Expected starttime to be truthy, got: " +  starttime)
        if not endtime:
            raise ArgumentError("Expected endtime to be truthy, got: " +  endtime)
        if not description:
            raise ArgumentError("Expected description to be truthy, got: " +  description)
        if not instructor:
            raise ArgumentError("Expected instructor to be truthy, got: " +  instructor)
        if not category:
            raise ArgumentError("Expected category to be truthy, got: " +  category)
        if not spaces:
            raise ArgumentError("Expected spaces to be truthy, got: " +  spaces)

        self.name = name
        self.starttime = starttime
        self.endtime = endtime
        self.description = description
        self.instructor = instructor
        self.category = category
        self.spaces = spaces

        self.session = session or requests.Session()

        self.current_classes = ListClasses(session=self.session)

        add_class_html = self.session.get(self.add_class_url).text

        self.soup = bs4.BeautifulSoup(add_class_html, 'html.parser')

        self.form = self.soup.find('form', id='post')

        if self.form is None:
            LOGGING.critical('cannot find form for "add class"')
            sys.exit(1)

        self.hidden_form_inputs = self.form.findAll(lambda t: t.name == 'input' and t.get('type') == 'hidden')
        self.event_categories = self.form.find(id='event-categories-all')
        self.post_id = self.form.find('input', id='post_ID').get('value')

        self._instructors = None

    @property
    def all_instructors(self):
        if self._instructors:
            return self._instructors

        params = {}

        params['post_id'] = self.post_id
        params['action'] = 'acf/fields/post_object/query'

        params['field_key'] = self.form.find(lambda t: t.name == 'div' and t.has_attr('data-key')).get('data-key')

        nonce_regex = re.compile('"nonce":"(?P<nonce>[^"]+)"')
        ajax_regex = re.compile('"ajaxurl":"(?P<ajax>[^"]+)"')

        for script in self.soup.findAll('script'):
            nonce_match = nonce_regex.search(script.text)
            if nonce_match:
                params['nonce'] = nonce_match.groupdict()['nonce']
                break

            ajax_match = ajax_regex.search(script.text)
            if ajax_match:
                ajax_url = ajax_match.groupdict()['ajax'].replace('\\', '')

        resp = self.session.post(ajax_url, data=params)
        resp.raise_for_status()

        self._instructors = resp.json()['results']
        return self._instructors

    def get_instructor_id(self, name):
        for instructor in self.all_instructors:
            if name.lower() in instructor['text'].lower():
                return instructor['id']

    def create_class(self):

        def find_category(category):
            if category.text.lower().strip() == self.category.lower():
                return True
            return False

        cat_id = self.event_categories.find(find_category).find('input')

        params = {}

        for input in self.hidden_form_inputs:
            params[input.get('name')] = input.get('value')

        params[cat_id.get('name')] = cat_id.get('value')
        params['acf[field_5640dca0a5364]'] = self.get_instructor_id(self.instructor)
        params['em_tickets[1][ticket_name]'] = 'Standard Ticket'
        params['em_tickets[1][ticket_spaces]'] = self.spaces
        # params['em_tickets[1][ticket_description]'] =
        params['em_tickets[1][ticket_price]'] =  1
        params['em_tickets[1][ticket_min]'] =  1
        params['event_rsvp'] = 1
        # params['em_tickets[1][ticket_max]'] =
        # params['ticket_start_pub'] =
        # params['em_tickets[1][ticket_start]'] =
        # params['em_tickets[1][ticket_start_recurring_days]'] =
        params['em_tickets[1][ticket_start_recurring_when]'] =  'before'
        # params['em_tickets[1][ticket_start_time]'] =
        params['ticket_end_pub'] =  self.starttime.strftime('%d/%m/%Y')
        params['em_tickets[1][ticket_end]'] =  self.starttime.strftime('%Y-%m-%d')
        # params['em_tickets[1][ticket_end_recurring_days]'] =
        params['em_tickets[1][ticket_end_recurring_when]'] =  'before'
        params['em_tickets[1][ticket_end_time]'] =  self.starttime.strftime('%I:%M %p')
        params['em_tickets[1][ticket_type]'] =  'members'
        params['content'] = self.description
        params['event_start_date'] = self.starttime.strftime('%Y-%m-%d')
        params['event_start_time'] = self.starttime.strftime('%I:%M %p')
        params['event_end_date'] = self.endtime.strftime('%Y-%m-%d')
        params['event_end_time'] = self.endtime.strftime('%I:%M %p')
        params['original_post_status'] = 'draft'
        params['save'] = 'Save Draft'
        params['hidden_post_status'] = 'draft'
        params['post_status'] = 'draft'
        params['post_title'] = "{} with {}".format(self.name.capitalize(), self.instructor.capitalize())
        params['post_ID'] = self.post_id

        resp = self.session.post(self.submit_class_form_url, data=params)
        resp.raise_for_status()

        return resp.ok
