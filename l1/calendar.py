import datetime
import textwrap
import uuid
import caldav
import re
import os
import logging

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
LOGGER.addHandler(logging.StreamHandler())


class Vevent:

    uid_regex = re.compile("^UID:(?P<UID>.*)$", re.MULTILINE)
    dtstamp_regex = re.compile(
        "^DTSTAMP(;[^:]+)?:(?P<DTSTAMP>[0-9]{8}T[0-9]{6})Z?$", re.MULTILINE
    )
    org_name_regex = re.compile(
        "^ORGANIZER;CN=(?P<ORG_NAME>[^:]+):MAILTO:(?P<ORG_EMAIL>.*)$", re.MULTILINE
    )
    org_email_regex = org_name_regex
    start_regex = re.compile("^DTSTART:(?P<START>[0-9]{8}T[0-9]{6})Z?$", re.MULTILINE)
    end_regex = re.compile("^DTEND:(?P<END>[0-9]{8}T[0-9]{6})Z?$", re.MULTILINE)
    summary_regex = re.compile("^SUMMARY:(?P<SUMMARY>[^:]+)$", re.MULTILINE)
    location_regex = re.compile("^LOCATION:(?P<LOCATION>[^:]+)$", re.MULTILINE)

    @classmethod
    def _dt_to_str(cls, dt):
        return dt.strftime("%Y%m%dT%H%M%S")

    @classmethod
    def event_from_raw(cls, vevent):

        params = {}

        for prm in [
            "uid",
            "dtstamp",
            "org_name",
            "org_email",
            "start",
            "end",
            "summary",
            "location",
        ]:
            regex = getattr(cls, "{}_regex".format(prm), None)
            m = regex.search(vevent)
            if m:
                params[prm] = m.group(prm.upper())

        for prm in ["dtstamp", "start", "end"]:
            params[prm] = datetime.datetime.strptime(params[prm], "%Y%m%dT%H%M%S")

        return Vevent(**params)

    def __init__(
        self,
        uid=None,
        dtstamp=None,
        org_name=None,
        org_email=None,
        start=None,
        end=None,
        summary=None,
        location=None,
    ):

        self.dtstamp = dtstamp or datetime.datetime.now()
        self.uid = uid or self._uid
        self.org_name = org_name
        self.org_email = org_email
        self.start = start
        self.end = end
        self.summary = summary
        self.location = location

    @property
    def _uid(self):
        fmt = "{ts}-{uuid}@l1.peteches.co.uk"
        return fmt.format(ts=self.dtstamp, uuid=uuid.uuid1().fields[0])

    def compare_event(self, othr):
        """Compares if two Vevents represent the same event.

        Note: does not check if uid or dtstamp match as the same event may have multiple calendar vevents
        """

        for elem in ["org_name", "org_email", "start", "end", "summary", "location"]:
            if getattr(self, elem) != getattr(othr, elem):
                return False

        return True

    def __str__(self):
        fmt = """
            BEGIN:VCALENDAR
            VERSION:2.0
            PRODID:-//peteches/l1_class_booking//EN
            BEGIN:VEVENT
            UID:{uid}
            DTSTAMP:{datetime}
            ORGANIZER;CN={org_name}:MAILTO:{org_email}
            DTSTART:{start}
            DTEND:{end}
            SUMMARY:{summary}
            LOCATION:{location}
            BEGIN:VALARM
            ACTION:DISPLAY
            TRIGGER:-30m
            DESCRIPTION:{summary}
            DURATION:10M
            REPEAT:3
            END:VALARM
            BEGIN:VALARM
            ACTION:AUDIO
            TRIGGER:-30M
            DESCRIPTION:{summary}
            DURATION:30M
            REPEAT:3
            END:VALARM
            END:VEVENT
            END:VCALENDAR
            """

        vevent = fmt.format(
            uid=self.uid,
            datetime=self._dt_to_str(self.dtstamp),
            org_name=self.org_name,
            org_email=self.org_email,
            start=self._dt_to_str(self.start),
            end=self._dt_to_str(self.end),
            summary=self.summary,
            location=self.location,
        )

        return textwrap.dedent(vevent)


class Calendar:
    def __init__(self, calendar, url, user, password):
        self.client = caldav.DAVClient(url, username=user, password=password)

        self.principal = self.client.principal()

        self.gym_cal = None
        for cal in self.principal.calendars():
            if cal.name == calendar:
                self.gym_cal = cal

    def get_events(self, frm, until):
        return [
            Vevent.event_from_raw(e.data)
            for e in self.gym_cal.date_search(start=frm, end=until)
        ]

    def add_event(self, event):
        event_obj = Vevent.event_from_raw(event)
        for e in self.get_events(frm=event_obj.start, until=event_obj.end):
            if event_obj.compare_event(e):
                LOGGER.info(
                    "Calendar event already in place for %s at %s",
                    event_obj.title,
                    event_obj.start,
                )
                return True
        try:
            self.gym_cal.add_event(event)
        except Exception as exp:
            LOGGER.warn("There was a problem adding event: %s", exp)
            raise exp
        else:
            return True
