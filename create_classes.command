#!/bin/bash -e

scriptdir=$(dirname $0)

cd ${scriptdir}
echo Pulling latest code changes.
git pull

if [[ -r ${scriptdir}/auth ]]; then
	echo sourceing auth details
	source ${scriptdir}/auth
fi

echo Creating classes based off last weeks schedule.
pipenv sync
pipenv run python class_maker.py

read -p 'You may now close the terminal window.'
