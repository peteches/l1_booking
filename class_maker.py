
#!/usr/bin/env python3

import datetime
import requests
import yaml
import configparser
import os
import sys
import logging
import re

import l1
import l1.calendar
import l1.classes

LOGGER = logging.getLogger("l1")
LOGGER.setLevel(logging.INFO)
LOGGER.addHandler(logging.StreamHandler())

session = requests.Session()

CONFIG_FILE = os.environ.get("L1_CONFIG", "l1_admin_classes.ini")
USER = os.environ.get("L1_ADMIN_USER", None)
PASS = os.environ.get("L1_ADMIN_PASS", None)

CONFIG = configparser.ConfigParser()
CONFIG.read(CONFIG_FILE)

if not (USER and PASS):
    print(
        "Error: Missing Auth details. Please Set both L1_ADMIN_USER and L1_ADMIN_PASS",
        file=sys.stderr,
    )
    sys.exit(1)

login_params = {
    "log": USER,
    "pwd": PASS,
    "action": "login",
    "wp-submit": "Log In",
}

login_url = "https://www.l1performance.com/login/"
r = session.post(login_url, data=login_params)
r.raise_for_status()

TODAY = datetime.datetime.today()
LAST_MONDAY = (TODAY - datetime.timedelta(days=TODAY.weekday())).replace(hour=0, minute=0, second=0)

DATES_PAST = [LAST_MONDAY - datetime.timedelta(weeks=x) for x in range(3)]
DATES_FUTURE = [LAST_MONDAY + datetime.timedelta(weeks=x) for x in range(3)]

def date_select(dates):
    for index, date in enumerate(dates):
        print('{}) {}'.format(index, date.strftime('%A %d %B')))

    try:
        selection = int(input('>> '))
    except ValueError as e:
        print('You should select a number.')
        sys.exit(1)

    try:
        return dates[selection]
    except IndexError as e:
        print('I was expecting a number between 0 and {}'.format(len(dates)-1))
        sys.exit(1)

print('Please select which week to copy classes from.')
week_from = date_select(DATES_PAST)

print('Please select which week to add classes to.')
week_start = date_select(DATES_FUTURE)

last_weeks_classes = [ c for c in l1.ListClasses(session=session).known_classes
                      if c['start'] > week_from and c['start'] < week_from + datetime.timedelta(days=7)]

title_regex = re.compile('(?P<name>.*) with (?P<instructor>.*)')

for l1_class in last_weeks_classes:
    match = title_regex.match(l1_class['title'])

    if not match:
        LOGGER.error('class %s does not match expected "name of class with instructor" format', l1_class['title'])
        continue

    cls_name = match.groupdict()['name'].lower()
    instructor = match.groupdict()['instructor']

    if not CONFIG.has_section(cls_name):
        LOGGER.warning('Could not find config for %s', cls_name)
        continue

    description = CONFIG.get(cls_name, 'description')
    category = CONFIG.get(cls_name, 'category')
    spaces = CONFIG.get(cls_name, 'spaces')

    starttime = (datetime.timedelta(days=l1_class['start'].weekday()) + week_start).replace(hour=l1_class['start'].hour, minute=l1_class['start'].minute)
    endtime = (datetime.timedelta(days=l1_class['start'].weekday()) + week_start).replace(hour=l1_class['end'].hour, minute=l1_class['end'].minute)

    cls = l1.classes.CreateClass(session=session,
                         name=cls_name,
                         starttime=starttime,
                         endtime=endtime,
                         description=description,
                         instructor=instructor,
                         category=category,
                         spaces=spaces)

    if cls.create_class():
        LOGGER.info("Successfully created %s on %s", cls_name, starttime)
    else:
        LOGGER.warning("Failed to create %s on %s", cls_name, starttime)

